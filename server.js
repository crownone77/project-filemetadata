'use strict';

const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const multer = require('multer');
const upload = multer({ dest: 'uploads/' });

const app = express();

app.use(cors());
app.use('/public', express.static(process.cwd() + '/public'));
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', function (req, res) {
  res.sendFile(process.cwd() + '/views/index.html');
});

app.post('/api/fileanalyse', upload.single('upfile'), (req, res) => {
  try {
    console.log(req.file);
    res.json({
      name: req.file.originalname,
      type: req.file.mimetype,
      size: req.file.size,
    });
  } catch (e) {
    console.error(e);
  }
});

app.get('/hello', function (req, res) {
  res.json({ greetings: 'Hello, API' });
});

const listener = app.listen(process.env.PORT || 3000, function () {
  console.log(`Node is running on port: ${listener.address().port}`);
});
